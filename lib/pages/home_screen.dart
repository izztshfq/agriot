import 'package:flutter/material.dart';
import 'farm_dashboard.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  /// Farm detail card, accepts farm objects
  Widget _buildFarmCard(context) {
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24, bottom: 25),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(16)),
        color: Color.fromARGB(255, 31, 32, 35),
        child: InkWell(
          splashColor: Colors.greenAccent.withAlpha(50),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => FarmDashboard()));
          },
          child: Container(
            height: 180,
            padding: const EdgeInsets.all(25),
            decoration: BoxDecoration(color: Colors.transparent, boxShadow: [
              BoxShadow(
                color: Color.fromARGB(21, 0, 0, 0),
                offset: Offset(0, 2),
                blurRadius: 48,
              )
            ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "My Farm",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: "Inter",
                            fontSize: 20,
                            letterSpacing: 0.5,
                          ),
                        ),
                        Text(
                          "Lot 420, Asajaya",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.white70,
                            fontWeight: FontWeight.w300,
                            fontFamily: "Inter",
                            fontSize: 15,
                            letterSpacing: 1,
                          ),
                        ),
                      ],
                    ),
                    Tooltip(
                      message: 'Connected',
                      height: 18,
                      child: Icon(
                        Icons.cloud_done,
                        color: Colors.greenAccent,
                      ),
                    ),
//                Tooltip(
//                  message: 'Could Not Connect',
//                  height: 18,
//                  child: Icon(
//                    Icons.error,
//                    color: Colors.redAccent,
//                  ),
//                )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _buildFarmStats("18", "Days Monitored"),
                    _buildFarmStats("5", "Notifications"),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Quick way to build stats for farm detail cards
  Widget _buildFarmStats(String value, String description) {
    return Column(
      children: <Widget>[
        Text(
          value,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: "Inter",
            fontSize: 24,
            letterSpacing: 0.5,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          description,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white70,
            fontWeight: FontWeight.w300,
            fontFamily: "Inter",
            fontSize: 15,
            letterSpacing: 1,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 31, 32, 35),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 0.0),
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                height: 80,
                margin: EdgeInsets.only(top: 100, right: 14, left: 14),
                child: Image.asset(
                  "assets/images/logo.png",
                  fit: BoxFit.fill,
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 286,
                  margin: EdgeInsets.only(top: 15, left: 24, bottom: 30),
                  child: Text(
                    "All your farms connected here ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 175, 128),
                      fontWeight: FontWeight.bold,
                      fontFamily: "Inter",
                      fontSize: 28,
                      letterSpacing: 0.8,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 45, 47, 53),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(24),
                          topRight: Radius.circular(24))),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 24, bottom: 4),
                          child: Text(
                            "Connected Farms",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: "Inter",
                              fontSize: 22,
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        child: ListView(
                          scrollDirection: Axis.vertical,
                          children: <Widget>[
                            _buildFarmCard(context),
                            _buildFarmCard(context),
                            _buildFarmCard(context),
                            _buildFarmCard(context),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Color.fromARGB(255, 0, 175, 128),
        icon: Icon(Icons.add),
        label: Text(
          "Add New Farm",
          style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Inter"),
        ),
        onPressed: () {
          print("Clicked");
        },
      ),
    );
  }
}
