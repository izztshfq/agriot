import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'home_screen.dart';
import 'farm_dashboard.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:agriot_app/component/chart_line.dart';

class SensorInfo extends StatefulWidget {
  @override
  _SensorInfoState createState() => new _SensorInfoState();
}

class _SensorInfoState extends State<SensorInfo> {
  Widget _sensorInfoCard(
      context, String sensorType, double value, IconData icon) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.all(
          Radius.circular(25),
        ),
      ),
      margin: const EdgeInsets.only(top: 20, left: 0, right: 0, bottom: 30),
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Icon(
                  icon,
                  size: 30,
                ),
              ),
              Text(sensorType,
                  style: TextStyle(
                    fontFamily: "Inter",
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  )),
//              Text(
//                value.toString(),
//                style: TextStyle(
//                  fontFamily: "Inter",
//                  fontSize: 30,
//                  fontWeight: FontWeight.w600,
//                ),
//              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Align(
                alignment: Alignment.centerRight,
                child: Text('Last Updated 12h Ago',
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.black45,
                      fontFamily: "Inter",
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    )),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text('DHT 11',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Inter",
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text('Virtual Pin 2',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Inter",
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    )),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      color: Color.fromARGB(255, 31, 32, 35),
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Color.fromARGB(255, 31, 32, 35),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            iconSize: 30.0,
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => FarmDashboard(),
              ),
            ),
          ),
          title: Text(
            'My Farm',
            style: TextStyle(fontFamily: "Inter"),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => HomeScreen(),
                ),
              ),
            ),
          ],
        ),
        backgroundColor: Color.fromARGB(255, 31, 32, 35),
        body: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Avg. Reading Today',
                    style: TextStyle(
                      fontFamily: "Inter",
                      color: Colors.white70,
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    '30.5 ℃',
                    style: TextStyle(
                      fontFamily: "Inter",
                      color: Colors.white70,
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
                  ),
                ],
//
              ),
              SizedBox(
                height: 15,
              ),
              LineChartSample1(),
              _sensorInfoCard(context, 'Soil Temperature', 32.1, Icons.ac_unit),
//            TopNavigation(),
//            ActionsRow(),
              // Settings(),
            ],
          ),
        ),
      ),
    );
  }
}
