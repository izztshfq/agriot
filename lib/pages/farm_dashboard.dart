import 'package:agriot_app/pages/home_screen.dart';
import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'sensor_info.dart';

class FarmDashboard extends StatefulWidget {
  @override
  _FarmDashboardState createState() => _FarmDashboardState();
}

class _FarmDashboardState extends State<FarmDashboard> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      color: Color.fromARGB(255, 31, 32, 35),
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Color.fromARGB(255, 31, 32, 35),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            iconSize: 30.0,
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => HomeScreen(),
              ),
            ),
          ),
          title: Text(
            'My Farm',
            style: TextStyle(fontFamily: "Inter"),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => HomeScreen(),
                ),
              ),
            ),
          ],
        ),
        backgroundColor: Color.fromARGB(255, 31, 32, 35),
        body: Column(
          children: <Widget>[
            TopNavigation(),
            ActionsRow(),
            // Settings(),
          ],
        ),
      ),
    );
  }
}

// TODO : Complete widget following other examples
class TopNavigation extends StatelessWidget {
  Widget _buildTopRowBg() {
    return Container(
      height: 110,
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 31, 32, 35),
      ),
      child: Stack(children: <Widget>[_buildWeather()]),
    );
  }

  Widget _topAppTitle() {
    return Center(
      child: Txt(
        'My Farm',
        style: titleStyle.clone()
          ..alignmentContent.topCenter()
          ..margin(top: 40, bottom: 0)
          ..fontSize(18)
          ..letterSpacing(0.8)
          ..fontWeight(FontWeight.w400),
      ),
    );
  }

  Widget _buildWeather() {
    return Container(
      margin: EdgeInsets.only(top: 25, bottom: 15),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Parent(
                    style: weatherIconStyle,
                    child: Icon(
                      Icons.wb_cloudy,
                      color: Colors.white,
                    ),
                  ),
                  Txt(
                    'Cloudy',
                    style: descriptionStyle,
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Txt(
                    '32 ℃',
                    style: titleStyle,
                  ),
                  Txt(
                    'Temperature',
                    style: descriptionStyle,
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Txt(
                    '32 %',
                    style: titleStyle,
                  ),
                  Txt(
                    'Humidity',
                    style: descriptionStyle,
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Text(
              'Data fetched from OpenWeatherMap for Kota Samarahan, MY',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w100,
                  fontStyle: FontStyle.italic,
                  color: Colors.white,
                  fontSize: 10),
            ),
          ),
        ],
      ),
    );
  }

  final topCardStyle = ParentStyle().clone()
    ..margin(top: 0)
    ..padding(horizontal: 0, vertical: 0)
    ..overflow.scrollable()
    ..alignment.center();

  final titleStyle = TxtStyle()
    ..fontFamily('Inter')
    ..textColor(Colors.white)
    ..fontSize(22)
    ..fontWeight(FontWeight.w700)
    ..margin(bottom: 0)
    ..alignmentContent.center();

  final descriptionStyle = TxtStyle()
    ..fontFamily('Inter')
    ..textColor(Colors.white)
    ..fontSize(13)
    ..fontWeight(FontWeight.w300)
    ..margin(bottom: 0)
    ..letterSpacing(1)
    ..alignmentContent.center();

  final ParentStyle weatherIconStyle = ParentStyle()
    ..alignmentContent.center()
    ..width(27)
    ..height(27)
    ..margin(bottom: 0);

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: topCardStyle,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildTopRowBg(),
        ],
      ),
    );
  }
}

class ActionsRow extends StatelessWidget {
  Widget _buildActionsItem(String title, IconData icon) {
    return Parent(
      style: actionsItemStyle,
      child: Column(
        children: <Widget>[
          Parent(
            style: actionsItemIconStyle,
            child: Icon(icon, size: 20, color: Color(0xFF42526F)),
          ),
          Txt(title, style: actionsItemTextStyle)
        ],
      ),
    );
  }

  Widget _buildSensorReadingCard(context, String sensorType, double value, IconData icon){
    return InkWell(
      splashColor: Colors.greenAccent.withAlpha(50),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SensorInfo()));
      },
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.all(
            Radius.circular(25),
          ),
        ),
        margin: const EdgeInsets.only(left: 10,right: 10,bottom: 10),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Icon(icon,size: 30,),
                ),
                Text(value.toString(),style: TextStyle(fontFamily: "Inter", fontSize: 30, fontWeight: FontWeight.w600,),)
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                    sensorType,
                    style: TextStyle(fontFamily: "Inter", fontSize: 15, fontWeight: FontWeight.w400,)
                ),
              ],
            )

          ],
        ),
      ),
    );
  }

  Widget _buildDeviceCard(String sensorType, double value, IconData icon){
    return Container(
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.all(
          Radius.circular(25),
        ),
      ),
      margin: const EdgeInsets.only(left: 10,right: 10,bottom: 10),
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Icon(icon,size: 30,),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(Icons.brightness_1, size: 15, color: Colors.black54),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                  sensorType,
                  style: TextStyle(fontFamily: "Inter", fontSize: 15, fontWeight: FontWeight.w400,)
              ),
              Text(
                  'Off',
                  style: TextStyle(fontFamily: "Inter", fontSize: 15, fontWeight: FontWeight.bold,color: Colors.black54)
              ),
            ],
          )

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Color.fromARGB(255, 31, 32, 35),
        child: Container(
          decoration: BoxDecoration(
              color: Color.fromARGB(255, 45, 47, 53),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24), topRight: Radius.circular(24))),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'SENSORS',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontFamily: "Inter",
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: Colors.white70),
                  ),
                ),
              ),
              GridView.count(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  mainAxisSpacing: 5,
                  childAspectRatio: MediaQuery.of(context).size.height / 600,
                  padding: EdgeInsets.all(15),
                  children: <Widget>[
                    _buildSensorReadingCard(context,'Soil Temperature',32.1, Icons.ac_unit),
                    _buildSensorReadingCard(context,'Soil Moisture', 70.8, Icons.blur_on),
                    _buildSensorReadingCard(context,'Light', 25, Icons.lightbulb_outline),
                    _buildSensorReadingCard(context,'Soil pH', 8, Icons.format_color_reset),

                  ]),
          Padding(
            padding: const EdgeInsets.only(left: 25.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'DEVICES',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontFamily: "Inter",
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white70),
              ),
            ),
          ),
              GridView.count(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  mainAxisSpacing: 5,
                  childAspectRatio: MediaQuery.of(context).size.height / 600,
                  padding: EdgeInsets.all(15),
                  children: <Widget>[
                    _buildDeviceCard('Irrigation Pump #1',32.1, Icons.dialpad),
                    _buildDeviceCard('Nutrient Pump #1', 70.8, Icons.blur_on),

                  ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _buildActionsItem('Sensor 1', Icons.wb_incandescent),
                  _buildActionsItem('Sensor 2', Icons.wb_incandescent),
                  _buildActionsItem('Sensor 3', Icons.wb_incandescent),
                  _buildActionsItem('Sensor 4', Icons.wb_incandescent),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  final ParentStyle actionsItemIconStyle = ParentStyle()
    ..alignmentContent.center()
    ..width(50)
    ..height(50)
    ..margin(bottom: 5)
    ..borderRadius(all: 30)
    ..background.hex('#F6F5F8')
    ..ripple(true);

  final ParentStyle actionsItemStyle = ParentStyle()
    ..background.color(Colors.transparent)
    ..margin(vertical: 20.0);

  final TxtStyle actionsItemTextStyle = TxtStyle()
    ..textColor(Colors.white)
    ..fontSize(12);
}

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(255, 45, 47, 53),
      child: Column(
        children: <Widget>[
          SettingsItem(Icons.location_on, hex('#8D7AEE'), 'Address',
              'Ensure your harvesting address'),
          SettingsItem(Icons.lock, hex('#F468B7'), 'Privacy',
              'System permission change'),
          SettingsItem(Icons.menu, hex('#FEC85C'), 'General',
              'Basic functional settings'),
          SettingsItem(Icons.notifications, hex('#5FD0D3'), 'Notifications',
              'Take over the news in time'),
          SettingsItem(Icons.question_answer, hex('#BFACAA'), 'Support',
              'We are here to help'),
        ],
      ),
    );
  }
}

class SettingsItem extends StatefulWidget {
  SettingsItem(this.icon, this.iconBgColor, this.title, this.description);

  final IconData icon;
  final Color iconBgColor;
  final String title;
  final String description;

  @override
  _SettingsItemState createState() => _SettingsItemState();
}

class _SettingsItemState extends State<SettingsItem> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: settingsItemStyle(pressed),
      gesture: Gestures()
        ..isTap((isTapped) => setState(() => pressed = isTapped)),
      child: Row(
        children: <Widget>[
          Parent(
            style: settingsItemIconStyle(widget.iconBgColor),
            child: Icon(widget.icon, color: Colors.white, size: 20),
          ),
          SizedBox(width: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Txt(widget.title, style: itemTitleTextStyle),
              SizedBox(height: 5),
              Txt(widget.description, style: itemDescriptionTextStyle),
            ],
          )
        ],
      ),
    );
  }

  final settingsItemStyle = (pressed) => ParentStyle()
    ..elevation(pressed ? 0 : 50, color: Colors.grey)
    ..scale(pressed ? 0.95 : 1.0)
    ..alignmentContent.center()
    ..height(70)
    ..margin(vertical: 10)
    ..borderRadius(all: 15)
    ..background.hex('#ffffff')
    ..ripple(true)
    ..animate(150, Curves.easeOut);

  final settingsItemIconStyle = (Color color) => ParentStyle()
    ..background.color(color)
    ..margin(left: 15)
    ..padding(all: 12)
    ..borderRadius(all: 30);

  final TxtStyle itemTitleTextStyle = TxtStyle()
    ..bold()
    ..fontSize(16);

  final TxtStyle itemDescriptionTextStyle = TxtStyle()
    ..textColor(Colors.black26)
    ..bold()
    ..fontSize(12);
}
