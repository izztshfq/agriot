import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'package:agriot_app/pages/farm_dashboard.dart';
import 'package:agriot_app/pages/home_screen.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      home: Scaffold(
//        body: Column(
//          children: <Widget>[
//            TopNavigation(),
//            Expanded(child: UserPage()),
//          ],
//        ),
//      ),
//    );
//  }

  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'AgroIOT',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.black12,
        primaryColor: Colors.greenAccent,
      ),
      home: HomeScreen(),
    );
  }
}

class UserPage extends StatelessWidget {
  //Define styles for contents and title
  final contentStyle = (BuildContext context) => ParentStyle()
    ..overflow.scrollable()
    ..padding(vertical: 30, horizontal: 20)
    ..minHeight(MediaQuery.of(context).size.height - (2 * 30));

  final titleStyle = TxtStyle()
    ..fontFamily('Quicksand')
    ..bold()
    ..fontSize(28)
    ..margin(bottom: 50)
    ..alignmentContent.center();

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: contentStyle(context),
      child: Column(
        children: <Widget>[
          UserCard(),
          // ActionsRow(),
          // Settings(),
        ],
      ),
    );
  }
}

// TODO : Complete widget following other examples
class TopNavigation extends StatelessWidget {
  Widget _buildTopRowBg() {
    return Container(
      height: 310,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
              fit: BoxFit.fill)),
      child: Stack(children: <Widget>[_topAppTitle(), _topTitle()]),
    );
  }

  Widget _topAppTitle() {
    return Center(
      child: Txt(
        'Agri IOT',
        style: titleStyle.clone()
          ..alignmentContent.topCenter()
          ..margin(top: 40, bottom: 0)
          ..fontSize(22)
          ..fontWeight(FontWeight.w700),
      ),
    );
  }

  Widget _topTitle() {
    return Container(
      child: Txt(
        'Farms',
        style: titleStyle,
      ),
    );
  }

  final topCardStyle = ParentStyle().clone()
    ..padding(horizontal: 0, vertical: 0)
    ..overflow.scrollable()
    ..alignment.center();

  final titleStyle = TxtStyle()
    ..fontFamily('Quicksand')
    ..bold()
    ..textColor(Colors.white)
    ..fontSize(32)
    ..fontWeight(FontWeight.w600)
    ..margin(bottom: 50)
    ..alignmentContent.center();

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: topCardStyle,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildTopRowBg(),
        ],
      ),
    );
  }
}

class UserCard extends StatelessWidget {
  Widget _buildUserRow(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Txt('Farm Site B', style: nameTextStyle),
            SizedBox(height: 5),
            Txt('Asajaya, Samarahan, Sarawak', style: nameDescriptionTextStyle)
          ],
        ),
        Parent(
          style: viewItemIconStyle,
          child: IconButton(
            icon: Icon(Icons.navigate_next),
            iconSize: 40,
            color: Colors.blueAccent,
            padding: const EdgeInsets.all(0),
            // TODO : Add link to Next Screen
            onPressed: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => FarmDashboard()))
            },
          ),
        )
      ],
    );
  }

  Widget _buildUserStats() {
    return Parent(
      style: userStatsStyle,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildUserStatsItem('846', 'Days Monitored'),
          _buildUserStatsItem('0', 'Notifications'),
        ],
      ),
    );
  }

  Widget _buildUserStatsItem(String value, String text) {
    final TxtStyle textStyle = TxtStyle()
      ..fontFamily('Quicksand')
      ..fontSize(20)
      ..fontWeight(FontWeight.w600)
      ..textColor(Colors.white);
    return Column(
      children: <Widget>[
        Txt(value, style: textStyle),
        SizedBox(height: 5),
        Txt(text, style: nameDescriptionTextStyle),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: userCardStyle,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[_buildUserRow(context), _buildUserStats()],
      ),
    );
  }

  //Styling

  final ParentStyle userCardStyle = ParentStyle()
    ..height(175)
    ..padding(horizontal: 20.0, vertical: 10)
    ..alignment.center()
    ..background.hex('#3977FF')
    ..borderRadius(all: 20.0)
    ..elevation(10, color: hex('#3977FF'));

  final ParentStyle userImageStyle = ParentStyle()
    ..height(50)
    ..width(50)
    ..margin(right: 10.0)
    ..borderRadius(all: 30)
    ..background.hex('ffffff');

  final ParentStyle userStatsStyle = ParentStyle()..margin(vertical: 10.0);

  final ParentStyle viewItemIconStyle = ParentStyle()
    ..alignmentContent.center()
    ..width(50)
    ..height(50)
    ..margin(bottom: 5)
    ..borderRadius(all: 30)
    ..background.hex('#F6F5F8')
    ..ripple(true);

  final TxtStyle nameTextStyle = TxtStyle()
    ..fontFamily('Quicksand')
    ..textColor(Colors.white)
    ..fontSize(18)
    ..fontWeight(FontWeight.w600);

  final TxtStyle nameDescriptionTextStyle = TxtStyle()
    ..fontFamily('Quicksand')
    ..textColor(Colors.white.withOpacity(0.9))
    ..fontSize(12);
}
