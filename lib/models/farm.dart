import 'package:agriot_app/models/device.dart';

class Farm {
  final String name;
  final String location;
  final int protocol;
  final bool isConnected;
  final List<Device> device;

  Farm({
    this.name,
    this.location,
    this.protocol,
    this.isConnected,
    this.device,
  });
}
