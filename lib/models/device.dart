import 'package:agriot_app/models/farm.dart';

class Device {
  final String name;
  final int pin;
  final String pinMode;
  final bool isTurnOn;
  final String color;

  Device({
    this.name,
    this.pin,
    this.pinMode,
    this.isTurnOn,
    this.color,
  });
}
