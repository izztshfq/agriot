import 'dart:async';
import 'counter_event.dart';

class CounterBloc {
  int _counter = 0;
  final _counterStateController = StreamController<int>();

  // Sink input and output to streams
  StreamSink<int> get _inCounter => _counterStateController.sink;
  // For State, expose only the stream 
  Stream<int> get counter => _counterStateController.stream;

  final _counterEventController = StreamController<CounterEvent>();
  // For event, expose only a sink which is an input
  Sink<CounterEvent> get counterEventSink => _counterEventController.sink;

  CounterBloc() {
    _counterEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState( CounterEvent event) {
    if (event is IncrementEvent)
    _counter++;
    else
    _counter--;

    _inCounter.add(_counter);
  }

  // close  stream controllers
  void dispose() {
    _counterEventController.close();
    _counterStateController.close();
  }
}